sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use restrict";
	return Controller.extend("com.bowdarkproducts.controller.Detail", {
		onInit: function() {
			var oRouter = this.getOwnerComponent().getRouter();
			oRouter.getRoute("Detail").attachMatched(this._onObjectMatched, this); 
		},
		/*
		onLoadProduct: function(oEvent) {
			var args = oEvent.getParameter("arguments");
			var sKey = this.getView().getModel().createKey("/ProductSet", {
				"ProductID": args.ProductId
			});
			
			this.getView().bindElement({ path: sKey,
				                         parameters: {expand: 'ToSupplier'} });
		},
		*/ 
			
		_onObjectMatched: function(oEvent) {
			//oArgs- productID
			var oArgs, oView; 
			oArgs = oEvent.getParameter("arguments"); 
			oView = this.getView(); 
			
			oView.bindElement({
				path : "/ProductSet('" + oArgs.productId + "')",
				 parameters: {
					expand: 'ToSupplier'
				}, 
				events: {
					change: this._onBindingChange.bind(this), 
					dataRequested: function(oEvent){
						oView.setBusy(false); 
					}
				}
			});
			
			//var oCtx = oView.getBindingContext();
		}, 
		
		
		getRouter: function(){
			return sap.ui.core.UIComponent.getRouterFor(this); 
		},
		
		
		_onBindingChange: function(oEvent){
			if(!this.getView().getBindingContext()){
				this.getRouter().getTargets.display("notFound"); 
			}
		},

		onBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				//var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				this.getRouter().navTo("app", {}, true);
			}
		}
	});
});