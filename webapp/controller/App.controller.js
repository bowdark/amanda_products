sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'jquery.sap.global'

], function(Controller, JSONModel, Filter, FilterOperator) {
	"use restrict";
	return Controller.extend("com.bowdarkproducts.controller.App", {
		onInit: function() {
			this._oView = this.getView();

			var oModel = this.getView().getModel();
			this._oMySmartTable = this.byId("mySmartTable");

			var oView = this.getView();
			oView.setModel(oModel);
			var oFilterBar = sap.ui.getCore().byId(oView.getId() + "--smartFilterBar");
			if (oFilterBar) {
				var that = this;
				oFilterBar.attachFilterChange(function(oEvent) {
					that.onFilterChanged();
				});

				oFilterBar.setBasicSearch(new sap.m.SearchField());

			}

		},

		myButtonPress: function(oEvent) {
			var oSelect = this.byId('mySelect');
			var controlSelectedKey = oSelect.getSelectedKey();
			var oJSModel = this.getView().getModel("myModel");
			var modelSelectedKey = oJSModel.getProperty("/selectedKey");
			sap.m.MessageBox.show(
				"From Control : " + controlSelectedKey + "\n" +
				"From Model : " + modelSelectedKey, {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: "Selected Key Values",
					actions: [sap.m.MessageBox.Action.OK]
				}
			);
		}, 

			onSearch: function(oEvent) {

			// add filter for search
			var aFilters = [];
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = new Filter("Name", sap.ui.model.FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}

			// update list binding
			var list = this.getView().byId("smartFilterBar");
			var binding = list.getBinding("items");
			binding.filter(aFilters, "Application");
		},

		onSelectionChange: function(oEvent) {

			var oList = oEvent.getSource();
			var oLabel = this.getView().byId("idFilterLabel");
			var oInfoToolbar = this.getView().byId("idInfoToolbar");

			// With the 'getSelectedContexts' function you can access the context paths
			// of all list items that have been selected, regardless of any current
			// filter on the aggregation binding.
			var aContexts = oList.getSelectedContexts(true);

			// update UI
			var bSelected = (aContexts && aContexts.length > 0);
			var sText = (bSelected) ? aContexts.length + " selected" : null;
			oInfoToolbar.setVisible(bSelected);
			oLabel.setText(sText);
		},

		onToggleSearchField: function(oEvent) {

			var oSearchField = this.oFilterBar.getBasicSearch();
			if (!oSearchField) {
				var oBasicSearch = new sap.m.SearchField({
					showSearchButton: false
				});
			} else {
				oSearchField = null;
			}

			this.oFilterBar.setBasicSearch(oBasicSearch);

			oBasicSearch.attachBrowserEvent("keyup", jQuery.proxy(function(e) {
				if (e.which === 13) {
					this.onSearch();
				}
			}, this));
		},

		toggleUpdateMode: function() {
			var oSmartFilterbar = this.getView().byId("smartFilterBar");
			var oButton = this.getView().byId("toggleUpdateMode");

			if (!oSmartFilterbar || !oButton) {
				return;
			}
		},

		onAfterVariantLoad: function(oEvent) {
			var oSmartFilterbar = this.getView().byId("smartFilterBar");

			if (oSmartFilterbar) {

				var oData = oSmartFilterbar.getFilterData();
				var oCustomFieldData = oData["_CUSTOM"];
				if (oCustomFieldData) {

					var oCtrl = oSmartFilterbar.determineControlByName("MyOwnFilterField");

					if (oCtrl) {
						oCtrl.setSelectedKey(oCustomFieldData.MyOwnFilterField);
					}
				}
			}
		},

		onBeforeVariantSave: function(oEvent) {
			if (oEvent.getParameter("context") === "STANDARD") {
				this._updateCustomFilter();
			}
		},

		onBeforeVariantFetch: function(oEvent) {
			this._updateCustomFilter();
		},

		_updateCustomFilter: function() {
			var oSmartFilterbar = this.getView().byId("smartFilterBar");

			if (oSmartFilterbar) {

				var oCtrl = oSmartFilterbar.determineControlByName("MyOwnFilterField");

				if (oCtrl) {
					oSmartFilterbar.setFilterData({
						_CUSTOM: {
							MyOwnFilterField: oCtrl.getSelectedKey()
						}
					});
				}
			}
		},

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		delete: function(oEvent) {

			var oItem = this.getView().byId("submitId");
			// var oItem2 = this.getView().byId("submitName");

			// if (oItem !== null) {
			var oContext = oItem.getValue();
			var oModel = this.getView().getModel();
			oModel.remove("/ProductSet('" + oContext + "')", null);
			// }

			// else {
			// 	var oContext2 = oItem2.getValue();
			// 	var oModel2 = this.getView().getModel();
			// 	oModel2.remove("/ProductSet('" + oContext2 + "')", null);
			// }

			var dataModel = this.getView().getModel("dataModel");
			if (dataModel) {
				dataModel.setData(null);
				dataModel.updateBindings(true);
			}
			this.getModel.refresh();
		},

		add: function(oEvent) {
			var oModel = this.getView().getModel();

			//var code = this.getView().byId("newTaxTarifCode").getValue();

			var oData = {
				// Need to have: Product Id, Type Code, Category, Name, SupplierID, Tax Tarif Code, Measure Unit, Currenecy Code
				ProductID: this.getView().byId("newId").getValue(),
				//TypeCode: $("#newTypeCode option:selected").text(), 
				TypeCode: this.getView().byId("newTypeCode").getSelectedKey(), 
				Category: this.getView().byId("newCategory").getSelectedKey(),
				Name: this.getView().byId("newName").getValue(), 
				Description: this.getView().byId("newDescription").getValue(), 
				TaxTarifCode: 1,
				SupplierID: '0100000000',
				MeasureUnit: "EA",
				CurrencyCode: this.getView().byId("newCurrencyCode").getSelectedKey()
					// Supplier: sap.ui.getCore().byId("newSupplier").getValue()
			};

			oModel.create("/ProductSet", oData);
			oModel.submitChanges();
			oModel.refresh();
		},

		onPress: function(oEvent) {
			var oItem = oEvent.getSource();
			var oContext = oItem.getBindingContext();

			this.getRouter().navTo("Detail", {
				productId: oContext.getObject("ProductID")
			});
		}
	});
});