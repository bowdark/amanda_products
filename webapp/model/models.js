sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/odata/ODataModel",
	"sap/ui/model/xml/XMLModel",
	"sap/ui/Device"
], function(JSONModel, ODataModel, Device) {
	"use strict";
	
	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		}
		
	};
});