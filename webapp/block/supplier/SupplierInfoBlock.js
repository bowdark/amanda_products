sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
	"use strict";
	return BlockBase.extend("com.bowdarkproducts.block.supplier.SupplierInfoBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "com.bowdarkproducts.block.supplier.SupplierInfo",
					type: "XML"
				},
				Expanded: {
					viewName: "com.bowdarkproducts.block.supplier.SupplierInfo",
					type: "XML"
				}
			}
		}	
	});
}, true);