sap.ui.define(["sap/uxap/BlockBase"], function(BlockBase) {
	"use strict";
	return BlockBase.extend("com.bowdarkproducts.block.product.ProductInfoBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "com.bowdarkproducts.block.product.ProductInfo",
					type: "XML"
				},
				Expanded: {
					viewName: "com.bowdarkproducts.block.product.ProductInfo",
					type: "XML"
				}
			}
		}	
	});
}, true);