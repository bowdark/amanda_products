sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/bowdarkproducts/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("com.bowdarkproducts.Component", {

		metadata: {
			manifest: "json"
		},

		init: function() {
			UIComponent.prototype.init.apply(this, arguments);
			
			//set the device model 
			this.setModel(models.createDeviceModel(), "device");

			// Parse the current url and display the targets of the route that matches the hash
			this.getRouter().initialize();
		}

	});
});